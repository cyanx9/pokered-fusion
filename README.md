This is a disassembly of Pokémon Red and Blue modified to work with my fusion randomizer.

To set up the repository, see [**INSTALL.md**](INSTALL.md).


- [**Original Pokémon Red Disassembly**][pokered]
- [**Pokémon Red-Blue Fusion Randomizer**][randomizer]

[pokered]: https://github.com/pret/pokered
[randomizer]: https://gitlab.com/cyanx9/pokered-fusion-randomizer